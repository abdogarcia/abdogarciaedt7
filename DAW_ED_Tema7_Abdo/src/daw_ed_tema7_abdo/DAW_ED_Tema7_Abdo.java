package daw_ed_tema7_abdo;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAW_ED_Tema7_Abdo {

    public static void main(String[] args) {
        System.out.println("\nCreant professor...");
        Professor p = new Professor(1, "Pep");
        
        System.out.println("\nCreant aules...");
        AulaGrup ag = new AulaGrup("A31", 40, "Baix dreta", "1DAW");
        AulaExamens ae = new AulaExamens("A31", 30, "Dalt dreta", 60);
        AulaInformatica ai = new AulaInformatica("A32", 50, "Dalt esquerra", 20);

        System.out.println("\nCreant reserves...");
        Reserva r1 = new Reserva(getData(), getHora(), p, ae);
        Reserva r2 = new Reserva(getData(), getHora(), p, ai);
        
        System.out.println("\nProfessor creat:");
        System.out.println(p);
        System.out.println("\nAules creades:");
        System.out.println(ag);
        System.out.println(ae);
        System.out.println(ai);
        System.out.println("\nReserves creades:");
        System.out.println(r1);
        System.out.println(r2);
    }

    public static Date getData() {
        DateFormat format = new SimpleDateFormat("DD/MM/YYYY");
        Date data = null;
        try {
            data = format.parse("12/03/2019"); // Es llegiria de teclat
        } catch (ParseException ex) {
            System.out.println("Error guardant la data");
        }
        return data;
    }
    
    public static Time getHora(){
        Time t = null;
        // ... Aconseguiríem una hora de teclat
        return t;        
    }

}
