package daw_ed_tema7_abdo;

import java.sql.Time;
import java.util.Date;

public class AulaExamens extends Aula implements Reservable {

  private int qPupitres;

    public int getqPupitres() {
        return qPupitres;
    }

    public void setqPupitres(int qPupitres) {
        this.qPupitres = qPupitres;
    }

    public AulaExamens(String codi, int m2, String lloc, int qPupitres) {
        super(codi, m2, lloc);
        this.qPupitres = qPupitres;
    }

    @Override
    public void netejar() {
        System.out.println("Netejant aula exàmens");
    }

    @Override
    public void reservar(Professor p, Date d, Time h) {
        System.out.println("Reservant aula exàmens");
        reserves.add(new Reserva(d, h, p, this));
    }

    @Override
    public String toString() {
        return super.toString() + "\nAulaExamens{" + "qPupitres=" + qPupitres + '}';
    }

}