package daw_ed_tema7_abdo;

import java.sql.Time;
import java.util.Date;

public class Reserva {

    private Date data;
    private Time hora;
    private Professor reservador;
    private Reservable reservat;

    public Reserva(Date data, Time hora, Professor reservador, Reservable reservat) {
        this.data = data;
        this.hora = hora;
        this.reservador = reservador;
        this.reservat = reservat;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public Professor getReservador() {
        return reservador;
    }

    public void setReservador(Professor reservador) {
        this.reservador = reservador;
    }

    public Reservable getReservat() {
        return reservat;
    }

    public void setReservat(Reservable reservat) {
        this.reservat = reservat;
    }

    @Override
    public String toString() {
        return "Reserva{" + "data=" + data + ", hora=" + hora + ", reservador=" + reservador + ", reservat=" + reservat + '}';
    }

 

}
