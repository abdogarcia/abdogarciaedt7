package daw_ed_tema7_abdo;

public abstract class Aula {
    protected String codi;
    protected int m2;

    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public int getM2() {
        return m2;
    }

    public void setM2(int m2) {
        this.m2 = m2;
    }

    public String getLloc() {
        return lloc;
    }

    public void setLloc(String lloc) {
        this.lloc = lloc;
    }
    protected String lloc;

    public Aula(String codi, int m2, String lloc) {
        this.codi = codi;
        this.m2 = m2;
        this.lloc = lloc;
    }
    public abstract void netejar();

    @Override
    public String toString() {
        return "Aula{" + "codi=" + codi + ", m2=" + m2 + ", lloc=" + lloc + '}';
    }
    
    
}
