package daw_ed_tema7_abdo;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;

public class AulaInformatica extends Aula implements Reservable {

    private int qOrdinadors;

    public int getqOrdinadors() {
        return qOrdinadors;
    }

    public void setqOrdinadors(int qOrdinadors) {
        this.qOrdinadors = qOrdinadors;
    }

    public AulaInformatica(String codi, int m2, String lloc, int qOrdinadors) {
        super(codi, m2, lloc);
        this.qOrdinadors = qOrdinadors;
    }

    @Override
    public void netejar() {
        System.out.println("Netejant aula informàtica");
    }

    @Override
    public void reservar(Professor p, Date d, Time h) {
        System.out.println("Reservant aula informàtica");
        reserves.add(new Reserva(d, h, p, this));
    }

    @Override
    public String toString() {
        return super.toString()+ "\nAulaInformatica{" + "qOrdinadors=" + qOrdinadors + '}';
    }

   
}
