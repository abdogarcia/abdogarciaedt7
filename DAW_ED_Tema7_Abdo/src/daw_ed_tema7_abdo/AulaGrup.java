package daw_ed_tema7_abdo;

public class AulaGrup extends Aula {

    public String getNomGrup() {
        return nomGrup;
    }

    public void setNomGrup(String nomGrup) {
        this.nomGrup = nomGrup;
    }

    private String nomGrup;

    public AulaGrup(String codi, int m2, String lloc, String nomGrup) {
        super(codi, m2, lloc);
        this.nomGrup = nomGrup;
    }   

    @Override
    public void netejar() {
        System.out.println("Netejant aula de grup");
    }

    @Override
    public String toString() {
        return super.toString()+ "\nAulaGrup{" + "nomGrup=" + nomGrup + '}';
    }

}
