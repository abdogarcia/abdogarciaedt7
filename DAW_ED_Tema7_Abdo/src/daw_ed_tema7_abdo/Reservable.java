package daw_ed_tema7_abdo;

import java.util.ArrayList;
import java.util.Date;
import java.sql.Time;

public interface Reservable {
    ArrayList <Reserva> reserves = new ArrayList();

    public abstract void reservar(Professor p, Date d, Time h);


}
