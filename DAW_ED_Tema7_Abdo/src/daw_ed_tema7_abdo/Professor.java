package daw_ed_tema7_abdo;

import java.util.ArrayList;

public class Professor {
    private int id;
    private String nom;
    ArrayList<Reserva> reserves = new ArrayList();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Professor(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Professor{" + "id=" + id + ", nom=" + nom + ", reserves=" + reserves + '}';
    }

    
}
